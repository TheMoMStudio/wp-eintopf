(function ($) {
	"use strict";

	/**
	 * All of the code for your public-facing JavaScript source
	 * should reside in this file.
	 *
	 * Note: It has been assumed you will write jQuery code here, so the
	 * $ function reference has been prepared for usage within the scope
	 * of this function.
	 *
	 * This enables you to define handlers, for when the DOM is ready:
	 *
	 * $(function() {
	 *
	 * });
	 *
	 * When the window is loaded:
	 *
	 * $( window ).load(function() {
	 *
	 * });
	 *
	 * ...and/or other possibilities.
	 *
	 * Ideally, it is not considered best practise to attach more than a
	 * single DOM-ready or window-load handler for a particular page.
	 * Although scripts in the WordPress core, Plugins and Themes may be
	 * practising this, we should strive to set a better example in our own work.
	 */

	$(window).load(function () {
		$(".eintopf-tab").on("click", function (e) {
			const el = $(this);
			const day = el.attr("data-day");
			$(".eintopf-timetable").addClass("eintopf-hidden");
			const timetable = $(`#day-${day}`).removeClass("eintopf-hidden");

			$(".eintopf-tab").removeClass("eintopf-active");
			el.addClass("eintopf-active");
		});

		$(".eintopf-event").on("click", function (e) {
			const el = $(this);
			// Close all but the clicked event component.
			$(".eintopf-event").each(function () {
				if (!$(this).is(el)) {
					$(this).removeClass("open");
				}
			});
			if (el.hasClass("open")) {
				el.removeClass("open");
			} else {
				el.addClass("open");
			}
		});
	});
})(jQuery);

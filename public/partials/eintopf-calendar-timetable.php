<?php

/**
 * Provide a public-facing view for the plugin
 *
 * This file is used to markup the public-facing aspects of the plugin.
 *
 * @link       https://maris.beer/
 * @since      1.0.0
 *
 * @package    Eintopf_Calendar
 * @subpackage Eintopf_Calendar/public/partials
 */

?>
<div class="eintopf-container">
	<div class="eintopf-tabs">
		<?php foreach($days as $key => $day) : ?>
			<div class="eintopf-tab<?php if ($day["active"]) : ?> eintopf-active<?php endif ?>" data-day="<?php echo $key ?>">
				<?php echo $day["title"] ?>
			</div>
		<?php endforeach ?>
	</div>
	<?php foreach($days as $key => $day) : ?>
		<div
			id="day-<?php echo $key?>"
			class="eintopf-timetable  cols-<?php echo count($places); ?> <?php if (!$day["active"]) : ?> eintopf-hidden<?php endif ?>"
			style="height: <?php echo $day["height"] + 50 ?>px"
		>
			<div class="eintopf-places cols-<?php echo count($places); ?>">
				<?php foreach($places as $i => $place) : ?>
					<div class="col col-<?php echo $i + 1 ?>">
						<?php echo $place ?>
					</div>
				<?php endforeach ?>
			</div>
			<div class="eintopf-times">
				<?php while ($day["start"]->getTimestamp() < $day["end"]->getTimestamp()) : ?>
					<div style="top:<?php echo $this->timetableYOffset($day["start"], $startHour) ?>px">
						<?php echo $day["start"]->format("H:i") ?>
					</div>
					<?php $day["start"]->add(new DateInterval("PT60M")) ?>
				<?php endwhile ?>
			</div>
			<div class="eintopf-events">
				<?php foreach($day["events"] as $event) : ?>
					<div
						id="<?php echo $event->id; ?>"
						class="h-event eintopf-event col col-<?php echo $event->col; ?> <?php echo implode(" ", $event->tags); ?>"
						style="top:<?php echo $event->top; ?>px;min-height:<?php echo $event->height; ?>px;height:<?php echo $event->height; ?>px;"
						itemscope itemtype="https://schema.org/Event"
					>
						<h2 class="p-name" itemprop="name"><?php echo $event->name; ?></h2>
						<span class="eintopf-event-time">
							<time class="dt-start" itemprop="startDate" datetime="<?php echo $event->start->format(DateTime::ATOM); ?>">
								<?php echo $event->start->format('H:i'); ?>
							</time>
							<?php if ($event->end) : ?>
							-
							<time class="dt-end" itemprop="endDate" datetime="<?php echo $event->end->format(DateTime::ATOM); ?>">
								<?php echo $event->end->format('H:i'); ?>
							</time>
							<?php endif ?>
						</span>
						<div class="eintopf-info eintopf-organizers eintopf-more">
							<?php if (count($event->organizers) > 0) : ?>
								<div>Von:
									<?php foreach($event->organizers as $o => $organizer) : ?>
										<span itemprop="organizer" ><?php echo $organizer ?><?php if ($i+1 < count($event->organizers)) : ?>, <?php endif ?></span>
									<?php endforeach ?>
								</div>
							<?php endif ?>
						</div>
						<?php if ($event->location) : ?>
							<div class="eintopf-info eintopf-location eintopf-more">
								Ort: <span class="p-location" itemprop="location"><?php echo $event->location ?></span>
							</div>
						<?php endif ?>
						<div class="eintopf-info eintopf-involved eintopf-more">
							<?php if (count($event->involved) > 0) : ?>
								<div>Mit:
									<?php foreach($event->involved as $i => $involved) : ?>
										<span itemprop="contributor"><?php echo $involved->name ?></span><?php if ($i+1 < count($event->involved)) : ?>, <?php endif ?>
									<?php endforeach ?>
								</div>
							<?php endif ?>
						</div>
						<br />
						<div class="p-summary eintopf-info eintopf-description eintopf-more" itemprop="description">
							<?php echo $event->description ?>
						</div>
						<br />
						<div class="eintopf-info eintopf-involved eintopf-more">
							<?php foreach($event->involved as $i => $involved) : ?>
								<div itemprop="contributor"><?php echo $involved->description ?></div>
							<?php endforeach ?>
						</div>
					</div>
				<?php endforeach ?>
			</div>
		</div>
	<?php endforeach ?>
	<div class="eintopf-powered-by">powered by <a href="https://eintopf.info">EINTOPF</a></div>
</div>

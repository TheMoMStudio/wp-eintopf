<?php

/**
 * Fired during plugin activation
 *
 * @link       https://maris.beer/
 * @since      1.0.0
 *
 * @package    Eintopf_Calendar
 * @subpackage Eintopf_Calendar/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Eintopf_Calendar
 * @subpackage Eintopf_Calendar/includes
 * @author     Maris Beer <maris@stuttgartdorfuture.de>
 */
class Eintopf_Calendar_Activator
{

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate()
	{
		// !!! ONLY DEACTIVATED TO SIMPLIFY - NO PRODUCTION !!!
		// cron job every 20 minutes
		/* if (!wp_next_scheduled('eintopf_calendar_updater')) {
			wp_schedule_event(time(), 'twenty_minutes', 'eintopf_calendar_updater');
		}
		
		// setup table in database
		self::create_table();*/


	}

		/**
	 * Create custom table in database.
	 * 
	 * @since    1.0.0
	 */
	private static function create_table()
	{

		global $wpdb;
		$table_name = $wpdb->prefix . str_replace('-', '_',  'eintopf-calendar') . '_data';
		$charset_collate = $wpdb->get_charset_collate();
		$sql = "CREATE TABLE $table_name (
			id VARCHAR(36) NOT NULL,
			organizers JSON NOT NULL,
			title VARCHAR(256) NOT NULL,
			location JSON NOT NULL,
			description VARCHAR(2048) NOT NULL,
			start_date VARCHAR(64),
			end_date VARCHAR(64),
			image VARCHAR(128),
			link VARCHAR(128) NOT NULL,
			PRIMARY KEY (id)
		) $charset_collate;";
		require_once(ABSPATH . 'wp-admin/includes/upgrade.php');
		dbDelta($sql);
	}
}

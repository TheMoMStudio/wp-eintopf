<?php

/**
 * Define the internationalization functionality
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @link       https://maris.beer/
 * @since      1.0.0
 *
 * @package    Eintopf_Calendar
 * @subpackage Eintopf_Calendar/includes
 */

/**
 * Define the internationalization functionality.
 *
 * Loads and defines the internationalization files for this plugin
 * so that it is ready for translation.
 *
 * @since      1.0.0
 * @package    Eintopf_Calendar
 * @subpackage Eintopf_Calendar/includes
 * @author     Maris Beer <maris@stuttgartdorfuture.de>
 */
class Eintopf_Calendar_i18n {


	/**
	 * Load the plugin text domain for translation.
	 *
	 * @since    1.0.0
	 */
	public function load_plugin_textdomain() {
		load_plugin_textdomain(
			'eintopf-calendar',
			false,
			dirname( dirname( plugin_basename( __FILE__ ) ) ) . '/languages'
		);

	}



}
